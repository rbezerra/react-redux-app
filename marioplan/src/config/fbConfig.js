import firebase from "firebase/app";
import "firebase/firestore";
import "firebase/auth";

// Initialize Firebase
var config = {
  apiKey: "AIzaSyCYyhQc4q4iL7kGA7yNQdzUOFGI4_Oc4co",
  authDomain: "net-ninja-mario-plan.firebaseapp.com",
  databaseURL: "https://net-ninja-mario-plan.firebaseio.com",
  projectId: "net-ninja-mario-plan",
  storageBucket: "",
  messagingSenderId: "28993423865"
};

firebase.initializeApp(config);
firebase.firestore().settings({ timestampsInSnapshots: true });

export default firebase;
